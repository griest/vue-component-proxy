# Installation

### Direct Download / CDN

https://unpkg.com/vue-component-proxy/dist/vue-component-proxy

[unpkg.com](https://unpkg.com) provides NPM-based CDN links. The above link will always point to the latest release on NPM. You can also use a specific version/tag via URLs like https://unpkg.com/vue-component-proxy@0.0.0/dist/vue-component-proxy.js
 
Include vue-component-proxy after Vue and it will install itself automatically:

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-component-proxy/dist/vue-component-proxy.js"></script>
```

### NPM

    $ npm install vue-component-proxy

### Yarn

    $ yarn add vue-component-proxy

When used with a module system, you must explicitly install the `vue-component-proxy` via `Vue.use()`:

```javascript
import Vue from 'vue'
import VueComponentProxy from 'vue-component-proxy'

Vue.use(VueComponentProxy)
```

You don't need to do this when using global script tags.

### Dev Build

You will have to clone directly from GitHub and build `vue-component-proxy` yourself if
you want to use the latest dev build.

    $ git clone https://github.com/griest024/vue-component-proxy.git node_modules/vue-component-proxy
    $ cd node_modules/vue-component-proxy
    $ npm install
    $ npm run build
