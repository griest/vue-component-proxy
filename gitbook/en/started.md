# Getting Started

> We will be using [ES2015](https://github.com/lukehoban/es6features) in the code samples in the guide.


### HTML

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-component-proxy/dist/vue-component-proxy.js"></script>

<div id="#app">
  <!-- TODO: here the outputs -->
</div>
```

### JavaScript

```javascript
// If using a module system (e.g. via vue-cli), import Vue and VueComponentProxy and then call Vue.use(VueComponentProxy).
// import Vue from 'vue'
// import VueComponentProxy from 'vue-component-proxy'
// 
// Vue.use(VueComponentProxy)

// TODO: here the example

// Now the app has started!
new Vue({ }).$mount('#app')
```

Output the following:

```html
<div id="#app">
  <!-- TODO: here the outputs -->
</div>
```
