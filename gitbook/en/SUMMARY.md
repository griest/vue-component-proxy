# vue-component-proxy documentation

> A VueJS plugin to proxy access to a child components props, events, and methods.

**[CHANGELOG](https://github.com/griest024/vue-component-proxy/blob/master/CHANGELOG.md)**

- [Installation](installation.md)
- [Getting Started](started.md)
