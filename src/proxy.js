import { union } from 'underscore'

/**
 * Return a new object with only properties that are in list, if list is truthy
 *
 * @param  {Object} object The object to be filtered
 * @param  {Array} list    An array of strings representing the keys of the properties you want to keep
 * @return {Object}
 */
function filterObject (object, list) {
  const ret = {}
  list ? Object.keys(object).forEach(key => { if (list.includes(key)) ret[key] = object[key] }) : Object.assign(ret, object)
  return ret
}

export default function ({ for: baseComponent, props: propList, methods: methodList, events: eventList }) {
  let context = null // the component that is acting as the proxy
  return {
    props: filterObject(baseComponent.props, propList),
    methods: filterObject(baseComponent.methods, methodList),
    created () {
      context = this // here, `this` is the Vue component instance that is mixing this in, store it for later use
    },
    directives: {
      proxy: {
        bind (el, binding, vnode) {
          union(Object.keys(binding.modifiers).filter(e => binding.modifiers[e]), eventList).forEach(event => { // iterate through the list of event names to be proxied
            vnode.componentInstance.$on(event, evtData => context.$emit(event, evtData)) // add the event handler to the base component instance
          })
        }
      }
    }
  }
}
