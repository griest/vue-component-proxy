import proxy from './proxy'

function plugin (Vue, options = {}) {
  Vue.componentProxyFactory = proxy
}

plugin.version = '__VERSION__'

export default plugin

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(plugin)
}
