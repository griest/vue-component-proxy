# Vue Component Proxy

[![Coverage Status](https://coveralls.io/repos/github/griest024/vue-component-proxy/badge.svg?branch=master)](https://coveralls.io/github/griest024/vue-component-proxy?branch=master)
[![npm](https://img.shields.io/npm/v/vue-component-proxy.svg)](https://www.npmjs.com/package/vue-component-proxy)
[![vue2](https://img.shields.io/badge/vue-2.x-brightgreen.svg)](https://vuejs.org/)

A VueJS plugin to proxy access to a child component's props, events, and methods. This is basically unecessary with the release of Vue 2.4.

> This project is unmaintained. However, feel free to open issues and submit MRs.

## Overview

### The Problem

As you DRY up your code, VueJS component hierarchies can get quite deep. Let's say you have the following component tree:

```
            App
            / \
           /   \
         Foo   Bar
         /
        A
       /             
      B
     / \
    C   D
   /
  E
```

Now assume that `App` needs to be able to configure `E` with some options passed in as props.
The only way to do this is to pass those options to `Foo` which in turn passes them down the hierarchy to `E`.
The trouble is that this means that every single intermediate component must declare each prop and pass it down manually.
As the number of props and the length of the hierarchy grows, maintaining this becomes a monumental task.

The officially suggested solution it to simply declare a single prop `options` that is an object containing all the props you need to pass.
Unfortuntely this means that you lose validation capability (`required`, `type`, etc...) for those individual props that you are passing in the `options` prop.

Both events and methods suffer from the same problem: every intermediate component must manually emit each event that needs to be passed through.

### The Solution

Generate a mixin that declares all props and methods specified for you, and a directive that binds those props and emits the events on a child component instance.

## Installation

`npm install vue-component-proxy --save`

## Usage

### Importing
- Import globally

```
import componentProxy from 'vue-component-proxy/dist/vue-component-proxy.esm.js'
Vue.use(componentProxy)
```

*OR*

- Import locally

```
import componentProxyFactory from 'vue-component-proxy/proxy.js'
```

### Mixing In

```
import ChildComponent from './child'

let options = {
    // here you specify which props, events, and methods to proxy
}

const componentProxy = Vue.componentProxyFactory({for: ChildComponent, ...options}) // if imported globally
const componentProxy = componentProxyFactory({for: ChildComponent, ...options}) // if imported locally

export default {
    mixins: [componentProxy],
    components: [ChildComponent]
}
```

### Proxying an Instance

```
<child-component v-proxy></child-component>
```

## Features

### Implemented

- Proxy some props, events, or methods by passing their names in as options
- Proxy all by setting option to falsey
- Proxy none by setting option to empty array

### Planned
- Named proxies
- Prefix `proxy` directive with proxy name
- Custom directive names and prefixes
- Customize modifications to prop names

## :copyright: License

[MIT](http://opensource.org/licenses/MIT)
