const fs = require('fs')
const pack = require('../package.json')

// update installation.md
const installation = fs
  .readFileSync('./gitbook/installation.md', 'utf-8')
  .replace(
    /https:\/\/unpkg\.com\/vue-component-proxy@[\d.]+.[\d]+\/dist\/vue-component-proxy\.js/,
    'https://unpkg.com/vue-component-proxy@' + pack.version + '/dist/vue-component-proxy.js.'
  )
fs.writeFileSync('./gitbook/installation.md', installation)
